const webpack = require('webpack')
const path = require('path')
const UglifyjsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const inProduction = process.env.NODE_ENV == 'production'
const watch = process.argv.includes('--watch')
const sourceMap = ! inProduction
const publicPath = '/'
const plugins = [
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
  }),

  new MiniCssExtractPlugin({
    filename: "css/[name].css",
  }),

  new VueLoaderPlugin(),

  new HtmlWebpackPlugin(),
]

/**
 * Build config
 *
 * @type {Object}
 */
const buildConfig = {
  mode: 'development',

  entry: {
    app: [
      './src/js/app.js',
      './src/scss/app.scss',
    ],
  },

  output: {
    path: path.resolve('./public'),
    filename: 'js/[name].js',
    publicPath,
  },

  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      vue$: 'vue/dist/vue.esm.js',
    },
  },

  optimization: {
    minimizer: [
      new UglifyjsPlugin({
        sourceMap: true,
        uglifyOptions: {
          compress: {
            warnings: false,
            drop_console: true,
          },
          output: {
            comments: false
          },
        },
      }),

      new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.css$/,
        cssProcessor: require('cssnano'),
        cssProcessorOptions: {
          reduceIdents: false,
          discardComments: { removeAll: true },
        },
        canPrint: true,
      })
    ]
  },

  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.(js|vue)$/,
        exclude: /node_modules/,
        loader: "eslint-loader",
      },
      {
        test: /\.js$/,
        exclude: [
          /node_modules\/(?!foundation-sites)/,
        ],
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        test: /\.s[ac]ss$/,
        use: [
          'vue-style-loader',
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'resolve-url-loader',
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.vue/,
        loader: 'vue-loader',
        options: {
          loaders: {
            js: [
              {
                loader: 'babel-loader',
              },
            ],
          },
          esModule: false,
        },
      },
      {
        test: /\.(ttf|otf|woff2?|eot)|fonts?\/.*\.svg$/,
        loader: 'file-loader',
        options: {
          name: `fonts/[name].[ext]?[hash:10]`,
          publicPath,
        },
      },
      {
        test: /\.(png|jpe?g)$/,
        loader: 'file-loader',
        options: {
          name: `images/[name].[ext]?[hash:10]`,
          publicPath,
        },
      },
    ],
  },

  devtool: sourceMap ? 'source-map' : '(none)',

  watch,

  plugins,
}

module.exports = buildConfig
