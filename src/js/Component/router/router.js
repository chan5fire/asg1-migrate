import Index from '../Index.vue' ;
import AboutUs from '../AboutUs.vue' ;
import ContactUs from '../ContactUs.vue';
import NewsLetter from '../NewsLetter.vue';
import Course from '../Course.vue';
import StudentAchievement from '../StudentAchievement.vue';
import SchoolLife from '../SchoolLife.vue';
import Sitemap from '../Sitemap.vue';
import SearchResult from '../SearchResult.vue';
import SchoolAnnocement from '../SchoolAnnocement.vue';
import MessageList from '../MessageList.vue';
import AnnocementList from '../annocement_article/AnnocementList';
import AnnocementDetail from '../annocement_article/AnnocementDetail';
export default [
  { path: '', component: Index },
  { path: '/about_us', component: AboutUs },
  { path: '/contact_us', component: ContactUs },
  { path: '/news_letter', component: NewsLetter },
  { path: '/course', component: Course },
  { path: '/student_achievement', component: StudentAchievement },
  { path: '/school_life', component: SchoolLife },
  { path: '/sitemap', component: Sitemap },
  { path: '/search_result', component: SearchResult },
  { path: '/school_annocement', component: SchoolAnnocement,
    children: [
      {
        name: 'school-annocement/list',
        path: '',
        component: AnnocementList
      },
      {
        name: 'school-annocement/detail',
        path: ':id',
        component: AnnocementDetail
      },
    ] },
  { path: '/message_list', component: MessageList }
];
