// import Vue from 'vue';
// import Vuex from 'vuex';

// Vue.use(Vuex);
export default {
  namespaced: true,
  state: {
    annocement: [
      {
        id: 1,
        title: '台灣企業考察五天團',
        date: '2018-06-19',
        content: `日期  :
                  2018年7月2日至6日(5天)
                  目的  :
                  讓學生了解台灣升學情況及營商環境，爲將來升學及就業做好準備。
                  對象  :
                  修讀企業、會計與財務概論學生同學優先考慮，名額共20名。
                  費用  :
                  HKD4,500.00 (包括所有食住費、來回機票、交通、機場稅、旅遊保險， 入場費及導遊費) 。
                  主辦單位：
                  商業學會
                  負責老師：
                  黃鶴樓老師

                  考察團行程：
                  日期
                  行程參考内容
                  7月2日
                  華山文化創業園區 、 台北101大樓、信義誠品書店
                  7月3日
                  國立政治大學/輔仁大學（待定）、淡水老街、漁人碼頭、士林夜市
                  7月4日
                  當地商業機構/企業探訪（待定）、十分老街、中正紀念堂、西門町
                  7月5日
                  台北當代藝術博物館、郭元益糕餅博物館、鳳梨酥DIY、師大夜市
                  7月6日
                  琉傳天下藝術館、林口三井Outlet

                  *行程安排或有更改，以當地旅行社為準*
                  有興趣參加的同學，儘快向負責老師或以下商業學會幹事查詢：
                  鄭詠同(4S)、蘇恩彤(4S)、謝婥瑗(4S)、陳姵希(4E)`

      },
      {
        id: 2,
        title: '今年學校的兩件大事：外評與校慶',
        date: '2018-05-30',
        content: `今年學校的兩件大事：外評與校慶
                  今年，確是圓玄二中忙碌精采的一年，上學期是教育局校外評核，下學期則是30周年校慶。
                  感謝同事的認真準備及各持分者的支持，外評隊肯定了學校努力的成果，對學校多項工作予以讚賞，特別是學生表現方面，更有高度評價。報告謂「學生喜愛校園生活，體育、科研成就出眾，學業成績不俗」，又謂學生「淳樸受教，有禮守規，同儕相處融洽」，正正反映同學的優秀表現；另一方面，報告亦提到同學要提高對自我的期許，以建立遠大的個人理想和目標。對此，我希望各位同學多加留意，切勿低估自己，要有更強烈的上進心，推動自己創造美好前程。
                  外評為學校進行了檢視，肯定的地方我們要繼續持守、發揮；不足之處我們要尋求改善方法，藉此推動學校持續發展。
                  至於30周年校慶，是學校五年一度的盛事。感謝全體員生的貢獻和付出，校慶得以成功舉辦，典禮、展覽、特刊、小學參訪、各項比賽、亮燈、聚餐、接待、表演、主題曲、視藝記….. 一切一切都是師生的心血所在。在一片歡樂喜慶的氣氛下，我們共度了難忘的時刻。
                  我曾說：校慶是凝聚歸屬的經歷，當我看到師生的投入、家長的擁護、校友的熱情，深切感受到圓玄二中結聚了不同持分者的緣份和感情，在這三十年裏，撒下的種子在眾人的澆溉與栽種下，紛紛結出美好的果實。校慶中動人的畫面，我們一定存於腦海，回味再三。
                  兩件大事，同樣費神耗力，但同樣極具價值，令這個學年添上不平凡的姿采！`
      },
      {
        id: 3,
        title: '中四及中五企會財科同學參加工作坊',
        date: '2018-05-09',
        content: `中四及中五企會財科同學參加工作坊 日期：14.5.2018 (星期一)
                  時間： 上午11:00 – 下午1:00 活動名稱： 求〝積〞攻略工作坊
                  活動目的： 認識強積金、及早策劃未來生活
                  活動地點： 學校活動室 / G1室
                  負責老師： 黃鶴樓老師`
      }
    ]
  },
  getters: {
    newUpdatedAnn: state => {
      let newAnnocement = state.annocement.map((ann, index) => {
        let title = ann.title;
        if (index < 2) {
          title = 'New!' + ann.title;
        }
        return {
          title: title,
          date: ann.date,
          id: ann.id,
          content: ann.content,
        };
      });
      return newAnnocement;
    },
    getAnnocementObj: state => id => {
      let annocementIdObj = null;
      if (id <= state.annocement.length) {
        state.annocement.forEach(function (annObj) {
          if (annObj.id == id) {
            annocementIdObj = annObj;
          }
        });
      }
      return annocementIdObj;
    }
  },
  mutations: {
    cutAnnocementContent: state => {
      state.annocement.forEach(ann => {
        ann.content = 'this is test. This is not real';
      });
    }
  },
  actions: {
    cutAnnocementContent: context => {
      setTimeout(function () {
        context.commit('cutAnnocementContent');
      }, 4000);
    }
  }
};
