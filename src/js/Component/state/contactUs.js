import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default {
  namespaced: true,
  state: {
    info: []
  },
  getters: {
  },
  mutations: {
    addNewMessage: (state, payload) => {
      state.info.push({ name: payload.name, email: payload.email, phone: payload.phone,
      message: payload.message, selectedGender: payload.selectedGender,
      selectedSubject: payload.selectedSubject });
    },
    deleteMessage: (state, itemID) => {
      state.info.splice(itemID, 1);
    }
  },
  actions: {
    addNewMessage: (context, data) => {
      context.commit('addNewMessage', data);
    },
    deleteMessage: (context, data) => {
      context.commit('deleteMessage', data);
    }
  }
};
