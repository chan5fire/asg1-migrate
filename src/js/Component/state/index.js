import Vue from 'vue';
import Vuex from 'vuex';
import annocement from './annocement';
import contactUs from './contactUs';

Vue.use(Vuex);
const modules = {
  annocement,
  contactUs
};

export default new Vuex.Store({
  modules: modules,
  strict: false
});
