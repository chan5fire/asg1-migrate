import Vue from 'vue';
import app from './app.vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Route from './Component/router/router.js';
import store from './Component/state/index.js';
import $ from 'jquery';
import 'foundation-sites';

Vue.use(Vuex);
Vue.use(VueRouter);

// jQuery
window.jQuery = $;
window.$ = $;

$(document).ready(function () {
  $(document).foundation();
});

const router = new VueRouter({
  routes: Route,
  mode: 'history',
  linkExactActiveClass: 'is-active',
});
document.addEventListener("DOMContentLoaded", addElement());
function addElement () {

  const app = document.createElement("div");
  app.id = 'app';
  document.body.appendChild(app);

  createApp();
}

function createApp () {
  new Vue({
    store: store,
    el: '#app',
    template: '<app></app>',
    components: { app },
    router: router,
  });
}
